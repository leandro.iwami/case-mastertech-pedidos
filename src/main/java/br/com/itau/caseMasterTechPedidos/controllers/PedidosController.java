package br.com.itau.caseMasterTechPedidos.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.caseMasterTechPedidos.models.Pedidos;
import br.com.itau.caseMasterTechPedidos.services.PedidosService;

@RestController
public class PedidosController {
	
	@Autowired
	PedidosService pedidosService;
	
	@PostMapping("/{nomeProduto}")
	public void cadastrarPedidos(@RequestBody Pedidos pedidos, @PathVariable String nomeProduto) {
		pedidosService.cadastrar(pedidos, nomeProduto);
	}
	
	@GetMapping("/{id}")
	public Optional<Pedidos> consultarPedidos(@PathVariable int id) {
		return pedidosService.consultar(id);
	}

}
