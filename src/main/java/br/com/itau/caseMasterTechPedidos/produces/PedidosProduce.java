package br.com.itau.caseMasterTechPedidos.produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.caseMasterTechPedidos.viewobjects.SolicitaPagamento;

@RestController
@SpringBootApplication
public class PedidosProduce {

	
		@Autowired
		KafkaTemplate<Object, Object> kafkaTemplate;

		@PostMapping
		public void publicar(SolicitaPagamento pagamento) {
			kafkaTemplate.send("efetivapagamento", pagamento);
		}
		
		public static void main(String[] args) {
			SpringApplication.run(PedidosProduce.class, args);
		}
		
		class Message {
			private String content;
			
			public Message() {}
			
			public Message(String content) {
				this.content = content;
			}
			
			public void setContent(String content) {
				this.content = content;
			}
			
			public String getContent() {
				return content;
			}
		}

}
