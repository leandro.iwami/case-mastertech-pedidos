package br.com.itau.caseMasterTechPedidos.viewobjects;

public class RespostaPagamento {
	
	private int idPedido;
	private boolean pagamentoEfetivado;

	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public boolean isPagamentoEfetivado() {
		return pagamentoEfetivado;
	}
	public void setPagamentoEfetivado(boolean pagamentoEfetivado) {
		this.pagamentoEfetivado = pagamentoEfetivado;
	}
	
	

}
