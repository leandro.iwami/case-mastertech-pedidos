package br.com.itau.caseMasterTechPedidos.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.caseMasterTechPedidos.viewobjects.Catalogo;

@FeignClient(name="catalogo")
public interface CatalogoClient {
	
		@GetMapping("/{nomeProduto}")
		public Catalogo buscarCatalogoPorId(@PathVariable String nomeProduto) ;

}
