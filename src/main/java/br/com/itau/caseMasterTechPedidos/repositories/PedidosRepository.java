package br.com.itau.caseMasterTechPedidos.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.caseMasterTechPedidos.models.Pedidos;

public interface PedidosRepository extends CrudRepository<Pedidos, Integer> {

}
