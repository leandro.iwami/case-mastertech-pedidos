package br.com.itau.caseMasterTechPedidos.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.itau.caseMasterTechPedidos.services.PedidosService;
import br.com.itau.caseMasterTechPedidos.viewobjects.RespostaPagamento;

@Component
public class PagamentoListener {

	PedidosService pedidosService = new PedidosService();

	Logger logger = LoggerFactory.getLogger(PagamentoListener.class);

	@Bean
	public RecordMessageConverter converter() {
		return new StringJsonMessageConverter();
	}

	@KafkaListener(id = "consumer", topics = "repostapagamento")
	public boolean consumir(@Payload RespostaPagamento respostaPagamento) {
		
		pedidosService.atualizarStatusPagamento(respostaPagamento);
		
		return respostaPagamento.isPagamentoEfetivado();
		
	}

}
