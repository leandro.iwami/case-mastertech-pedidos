package br.com.itau.caseMasterTechPedidos.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.caseMasterTechPedidos.client.CatalogoClient;
import br.com.itau.caseMasterTechPedidos.models.Pedidos;
import br.com.itau.caseMasterTechPedidos.produces.PedidosProduce;
import br.com.itau.caseMasterTechPedidos.repositories.PedidosRepository;
import br.com.itau.caseMasterTechPedidos.viewobjects.Catalogo;
import br.com.itau.caseMasterTechPedidos.viewobjects.RespostaPagamento;
import br.com.itau.caseMasterTechPedidos.viewobjects.SolicitaPagamento;

@Service
public class PedidosService {
	
	@Autowired
	PedidosRepository pedidosRepository;
	
	@Autowired
	CatalogoClient catalogoClient;
	
	@Autowired
	PedidosProduce pedidosProduce;
	
	public Optional<Catalogo> cadastrar(Pedidos pedidos, String nomeProduto) {
		Optional<Catalogo> catalogoOptional = buscarCatalogo(nomeProduto);
		
		try {
			
			if(!catalogoOptional.isPresent() || !catalogoOptional.get().isDisponivel()) {
				return Optional.empty();
			}
			
		} catch (Exception e) {
			return Optional.empty();
		}
		pedidos.setNomeCurso(catalogoOptional.get().getNome());
		pedidos.setPreco(catalogoOptional.get().getPreco());
		pedidosRepository.save(pedidos);
		
		SolicitaPagamento solicitaPagamento = new SolicitaPagamento();
		
		solicitaPagamento.setIdPedido(pedidos.getId());
		realizarPagamento(solicitaPagamento);
		return catalogoOptional;
	}
	
	private Optional<Catalogo> buscarCatalogo(String nome) {
		try {
			Catalogo catalogo = catalogoClient.buscarCatalogoPorId(nome);	
			return Optional.of(catalogo);
		} catch (Exception e) {
			return Optional.empty();
		}
	}
	
	
	public Optional<Pedidos> consultar(int id) {
		Optional<Pedidos> pedidosOptional = pedidosRepository.findById(id);
		
		if(!pedidosOptional.isPresent()) {
			return Optional.empty();
		}	
		return pedidosOptional;
	}
	
	public void realizarPagamento(SolicitaPagamento pagamento) {
		pedidosProduce.publicar(pagamento);
		
	}
	
	public void atualizarStatusPagamento(RespostaPagamento respostaPagamento) {
		
		Optional<Pedidos> pedidosOptional = consultar(respostaPagamento.getIdPedido());
		
		if(pedidosOptional.isPresent()) {
			pedidosOptional.get().setStatusPagamento(respostaPagamento.isPagamentoEfetivado());
			pedidosRepository.save(pedidosOptional.get());
		}
		
	}

}
